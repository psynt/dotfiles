stty -ixon # Disable ^s & ^q
# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend
shopt -s cdspell
shopt -s dirspell
shopt -s extglob
shopt -s checkwinsize
shopt -s globstar
shopt -s autocd

HISTSIZE= HISTFILESIZE= # infinite history

# ibus settings
export GTK_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
export QT_IM_MODULE=ibus

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
#unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

export GTK_IM_MODULE_FILE=/etc/gtk-2.0/gtk.immodules
export GTK_IM_MODULE_FILE=/usr/lib/gtk-3.0/3.0.0/immodules.cache

# Git alias
alias rei='git clone'
alias gaa='git add -A'
alias gcm='git commit -m'
alias gcam='git commit -a -m'
alias gp='git push'
alias gra='git remote add'
alias grao='git remote add origin'
alias gi='git init'
alias ga='git add'
alias gc='git commit'
alias ggm='git merge'
alias gs='git status'
alias gpm='git push -u origin master'
alias gpd='git push -u origin dev'
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

alias py='/usr/bin/python3'
alias pyp='/home/kaede/.local/bin/pip3'

alias pas='php artisan serve&'

alias up='sudo apt update && sudo apt upgrade'
alias upb='sudo apt update && sudo apt upgrade -t stable-backports'
alias upd='sudo apt update && sudo apt dist-upgrade'
alias updb='sudo apt update && sudo apt dist-upgrade -t stable-backports'
alias updf='sudo apt update && sudo apt dist-upgrade -t buster-fasttrack'
alias lookapt='apt search'
alias instapt='sudo apt install'
alias reinstapt='sudo apt install --reinstall'
alias instbpo='sudo apt -t stable-backports install'
alias instft='sudo apt -t stable-fasttrack install'
alias mkdeb='checkinstall --install=no --backup=no -D'

alias vbash='vim ~/.bashrc'
alias vvim='vim ~/.vim/vimrc'

alias w2xg='waifu2x-converter-cpp -s -z --block-size 1024 -m noise --noise-level 3'
alias w2xc='waifu2x-converter-cpp -s -z --block-size 1024 -m noise --noise-level 3 -p 2'

alias down='systemctl poweroff'

export JAVA_HOME="/usr/lib/jvm/java-8-oracle"

export PATH="/home/kaede/.local/bin:$PATH"

export EDITOR=vim
export VISUAL=vim

neofetch

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="/home/kaede/.sdkman"
[[ -s "/home/kaede/.sdkman/bin/sdkman-init.sh" ]] && source "/home/kaede/.sdkman/bin/sdkman-init.sh"


# PS1="\n\[\033[3;44m\]\[\033[2;35m\]  楓荘 の  \[\033[7;35m\]\[\033[1m\]楓\[\033[00m\]\]\n:\[\033[1;37m\]\w\[\033[00m\]\$ "
PS1="\n\[\033[3;44m\]\[\033[2;35m\]\hの\[\033[7;35m\]\[\033[1m\]\u\[\033[00m\]\]\n:\[\033[7;37m\]\w\[\033[00m\] $"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# PS1="\n\[\033[3;44m\]\[\033[2;35m\]  楓荘 の  \[\033[7;35m\]\[\033[1m\]楓\[\033[00m\]\]\n:\[\033[1;37m\]\w\[\033[00m\]\$ "
# PS1="\n\[\033[3;44m\]\[\033[2;35m\]\hの\[\033[7;35m\]\[\033[1m\]\u\[\033[00m\]\]\n:\[\033[7;37m\]\w\[\033[00m\] $"

# Git prompt 
source ~/.git-prompt.sh

export GIT_PS1_SHOWDIRTYSTATE=1
export GIT_PS1_SHOWSTASHSTATE=1
export GIT_PS1_SHOWUNTRACKEDFILES=1
export GIT_PS1_SHOWUPSTREAM=1
PROMPT_COMMAND='__git_ps1 "\n\[\033[3;44m\]\[\033[2;35m\]\hの\[\033[7;35m\]\[\033[1m\]\u\[\033[00m\]\]" "\n:\[\033[7;37m\]\w\[\033[00m\] $"'
. "$HOME/.cargo/env"
